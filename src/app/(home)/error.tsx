"use client";

import { useEffect } from "react";

export default function Error({ error, reset }: ErrorPageProps) {
  useEffect(() => {
    console.log(error);
  }, []);
  return (
    <div
      style={{
        padding: "10rem",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <h2>🚨</h2>
      <p>ha ocurrrido un error.</p>
      <button onClick={reset}>Try again</button>
    </div>
  );
}
