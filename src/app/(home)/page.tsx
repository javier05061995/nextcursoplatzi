import { MainProducts } from "app/components/Home/MainProducts";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Future world",
  description: "welcome to the futre world, an ecomercer from other century",
  keywords: ["ecomerce", "future", "world", "techonology"],
};

export default function Home() {
  return (
    <main>
      <MainProducts />
    </main>
  );
}
