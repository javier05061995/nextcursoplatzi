import { getMainProducts } from "app/services/shopify/products";
import { NextResponse } from "next/server";

export async function GET() {
    const products = await getMainProducts();
    return NextResponse.json({ products });
}
