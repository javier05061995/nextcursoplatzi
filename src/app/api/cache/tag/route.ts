import { env } from "app/config/env";
import { revalidateTag } from "next/cache";
import { NextResponse } from "next/server";

export async function POST(request: Request) {
  const body = await request.json();
  const { tag, token } = body;
  if (!tag || !token) {
    return Response.json({ error: "mission tag or token" }, { status: 400 });
  }
  console.log(body);
  console.log(env);

  if (token !== env.CACHE_TOKEN) {
    return NextResponse.json({ error: "tokken erroneo" }, { status: 400 });
  }
  revalidateTag(tag);
  return Response.json({ success: true }, { status: 200 });
}
