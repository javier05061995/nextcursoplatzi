import { env } from "app/config/env";
import { revalidatePath } from "next/cache";
import { NextResponse } from "next/server";

export async function POST(request: Request) {
  const body = await request.json();
  const { path, token } = body;
  if (!path || !token) {
    return Response.json({ error: "mission path or token" }, { status: 400 });
  }
  console.log(body);
  console.log(env);

  if (token !== env.CACHE_TOKEN) {
    return NextResponse.json({ error: "tokken erroneo" }, { status: 400 });
  }
  revalidatePath(path);
  return Response.json({ success: true }, { status: 200 });
}
