import { ProductView } from "app/components/product/ProductView";
import { getProducts } from "app/services/shopify/products";
import { redirect } from "next/navigation";

interface ProductoPageProps {
  searchParams: {
    id: string;
  };
}
export async function generateMetadata({ searchParams }: ProductoPageProps) {
  const id = searchParams.id;
  const products = await getProducts(id);
  const product = products[0];

  return {
    title: product.title,
    description: product.description,
    keywords: product.tags,
    openGraph: {
      images: [product.image],
    },
  };
}

export default async function ProductPage({ searchParams }: ProductoPageProps) {
  const id = searchParams.id;
  const products = await getProducts(id);
  const product = products[0];
  if (!id) {
    redirect("/store");
  }
  return (
    <>
      <h1>Product Page</h1>
      <ProductView product={product} />
    </>
  );
}
