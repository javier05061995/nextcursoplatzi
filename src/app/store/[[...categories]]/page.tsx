import { ProductsWrapper } from "app/components/Store/ProductsWrapper";
import { getCollections, getCollectionsProducts } from "app/services/shopify/collections";
import { getProducts } from "app/services/shopify/products";

interface CategoryProps {
    params: {
        categories: string[];
    };
    searchParams: {
        search?: string;
    };
}
export default async function Category(props: CategoryProps) {
    const { categories } = props.params;
    const collections = await getCollections();
    let product = [];

    if (categories?.length > 0) {
        const selecteCollectionsId = collections.find((collections: SmartCollection) => collections.handle === categories[0]).id;
        product = await getCollectionsProducts(selecteCollectionsId);
    } else {
        product = await getProducts();
    }
    const { search } = props.searchParams;

    return <ProductsWrapper products={product} />;
}
