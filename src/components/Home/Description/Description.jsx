"use client";
import Image from "next/image";
import classNames from "classnames/bind";
import { useState } from "react";
import styles from "./Description.module.sass";
const placeholder =
  "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/4gHYSUNDX1BST0ZJTEUAAQEAAAHIAAAAAAQwAABtbnRyUkdCIFhZWiAH4AABAAEAAAAAAABhY3NwAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAQAA9tYAAQAAAADTLQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAlkZXNjAAAA8AAAACRyWFlaAAABFAAAABRnWFlaAAABKAAAABRiWFlaAAABPAAAABR3dHB0AAABUAAAABRyVFJDAAABZAAAAChnVFJDAAABZAAAAChiVFJDAAABZAAAAChjcHJ0AAABjAAAADxtbHVjAAAAAAAAAAEAAAAMZW5VUwAAAAgAAAAcAHMAUgBHAEJYWVogAAAAAAAAb6IAADj1AAADkFhZWiAAAAAAAABimQAAt4UAABjaWFlaIAAAAAAAACSgAAAPhAAAts9YWVogAAAAAAAA9tYAAQAAAADTLXBhcmEAAAAAAAQAAAACZmYAAPKnAAANWQAAE9AAAApbAAAAAAAAAABtbHVjAAAAAAAAAAEAAAAMZW5VUwAAACAAAAAcAEcAbwBvAGcAbABlACAASQBuAGMALgAgADIAMAAxADb/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/2wBDAQcHBwoIChMKChMoGhYaKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCgoKCj/wAARCAAfAB8DASIAAhEBAxEB/8QAGQAAAgMBAAAAAAAAAAAAAAAABAYBAwUC/8QAHxAAAgICAgMBAAAAAAAAAAAAAQIABAMFEzEREiGB/8QAFwEBAQEBAAAAAAAAAAAAAAAAAgMBBP/EABwRAAICAgMAAAAAAAAAAAAAAAACARESIgMTIf/aAAwDAQACEQMRAD8ARDjIhdLGSZ3mQAQjWqC4nQjWpnMnthRwHjmJfQq0cOIcX5FrbqA8CasDK4oHsWB47l2tsAOPsXnsEy+nnIYRVhBZW7IHoWhxdxd22UM8hbTencz7eUs32BdpItGJ/9k=";
export const Description = () => {
  const [border, setBorder] = useState(false);
  const handleClick = () => setBorder(!border);
  const cx = classNames.bind(styles);

  const buttonStyles = cx("Description__button", {
    "Description__button--border": border,
  });

  return (
    <section className={styles.Description}>
      <button onClick={handleClick} className={buttonStyles}>
        <div className={styles.Description__imageContainer}>
          <Image
            src="/images/description.jpeg"
            alt="products markeplace"
            fill
            placeholder="blur"
            blurDataURL={placeholder}
          />
        </div>
      </button>
      <div className={styles.Description__text}>
        <h2>descripcion</h2>
        <p>
          Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi, aspernatur eveniet beatae
          dolorem modi molestias culpa quod magnam nesciunt accusamus quos, maxime ab expedita
          perspiciatis. Quia temporibus atque officia exercitationem.
        </p>
      </div>
    </section>
  );
};
