"use client";

import { useChat } from "ai/react";
import styles from "./chat.module.sass";
export const Chat = (props: { agent: string }) => {
  console.log(props.agent);

  const { messages, input, handleInputChange, handleSubmit } = useChat({
    initialMessages: [
      {
        id: "1",
        role: "system",
        content: props.agent,
      },
    ],
  });

  return (
    <main className={styles.Chat}>
      <section>
        {messages
          .filter((m) => m.role !== "system")
          .map((m) => (
            <div key={m.id}>
              {m.role === "user" ? "User: " : "Javier: "}
              {m.content}
            </div>
          ))}
      </section>
      <form className="flex space-x-4" onSubmit={handleSubmit}>
        <input className="rounded-md p-2 text-black" value={input} onChange={handleInputChange} placeholder="Say something..." />
        <button className="border-solid border-2 border-white p-2 rounded-md" type="submit">
          enviar
        </button>
      </form>
    </main>
  );
};
